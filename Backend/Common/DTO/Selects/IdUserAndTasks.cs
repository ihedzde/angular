using System.Collections.Generic;
using System.Text;
using Common.DTO.Task;
using Common.DTO.User;

namespace Common.DTO.Selects
{
    public class IdUserAndTasks
    {
        public int Id { get; set; }
        public UserDTO User { get; set; }
        public IList<TaskDTO> Tasks { get; set; }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (var task in Tasks)
            {
                builder.AppendLine(task.ToString());
            }

            return $"Id: {Id} User: {User} Members: {builder.ToString()}";
        }
    }
}