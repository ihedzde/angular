namespace Common.DTO.Selects
{
    public struct IdAndName
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public override string ToString()
        {
            return $"Id: {Id} Name: {Name}";
        }
    }
}