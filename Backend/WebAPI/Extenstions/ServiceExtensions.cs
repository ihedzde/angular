using System.Collections.Generic;
using System.Reflection;
using BLL.Domain.Services;
using BLL.MappingProfiles;
using BLL.Services;
using DAL.Domain.Models;
using DAL.Domain.Repositories;
using DAL.Persistance.Context;
using DAL.Persistance.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace WebAPI.Extenstions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddTransient<AppDbContext>();
            services.AddTransient<IRepository<UserModel>, UserRepository>();
            services.AddTransient<IRepository<TeamModel>, TeamRepository>();
            services.AddTransient<IRepository<TaskModel>, TaskRepository>();
            services.AddTransient<IRepository<ProjectModel>, ProjectRepository>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ITeamService, TeamService>();
            services.AddTransient<ITaskService, TaskService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IAggregateServices, AggregateServices>();
        }

        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}