using System;
using System.ComponentModel.DataAnnotations.Schema;
using Common.DTO.Task;

namespace DAL.Domain.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        private TaskState state;
        public int ProjectId { get; set; }
        public ProjectModel Project { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string TLDR { get; set; }

        public TaskState State
        {
            get => state;
            set
            {
                state = value;
                if(state == TaskState.Finished){
                    FinishedAt = DateTime.Now;
                }
                else{
                    FinishedAt = null;
                }
            }
        }

        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
