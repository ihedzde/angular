using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;

namespace DAL.Domain.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task<IList<T>> ReadAll(CancellationToken? token);
        Task<T> Create(T data);
        Task<T> Read(int id);
        Task<T> Update(T data);
        Task<T> Delete(int id);
    }
}