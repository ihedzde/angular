using System;
using System.Net.Http;
using System.Threading.Tasks;
using WebAPI.Extenstions;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using Common.DTO.Project;
using Newtonsoft.Json;
using System.Text;
using System.Net;

namespace WebAPI.IntegrationTests
{
    public class ProjectControllerTests: IClassFixture<WebAPIFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly WebAPIFactory<Startup> _factory;
        public ProjectControllerTests(WebAPIFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions{
                AllowAutoRedirect = false,
                BaseAddress = new Uri("https://localhost:5001")
            });
        }
        [Fact]
        public async Task Post_CreateProject_WhenValidProject_ThanReturnsOk(){
            //Arrange
            var createPostDTO = new CreateProjectDTO{
                Name = "Binary Studio 2021",
                Description = "Finished splended tasks in time to deadline",
                Deadline = new System.DateTime(2021, 7, 6, 23, 59, 59),
                AuthorId = 1,
                TeamId = 2,
            };
            //Act
            var response = await _client.PostAsync("/api/project", new StringContent(JsonConvert.SerializeObject(createPostDTO), Encoding.UTF8, "application/json"));
            //Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
        [Fact]
        public async Task Post_CreateProject_WhenInValidProject_ThanReturnsBadRequest(){
            //Arrange
            var createPostDTO = new CreateProjectDTO{
                Name = "Binary Studio 2021",
                Description = "Finished splended tasks in time to deadline",
                Deadline = new DateTime(2021, 7, 6, 23, 59, 59),
                AuthorId = 10230,
                TeamId = 102032,
            };
            //Act
            var response = await _client.PostAsync("/api/project", new StringContent(JsonConvert.SerializeObject(createPostDTO), Encoding.UTF8, "application/json"));
            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
        
    }
}