using System;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;
using System.Net;

namespace WebAPI.IntegrationTests
{
    public class UserControllerTests: IClassFixture<WebAPIFactory<Startup>>
    {
         private readonly HttpClient _client;
        private readonly WebAPIFactory<Startup> _factory;
        public UserControllerTests(WebAPIFactory<Startup> factory)
        {
             _factory = factory;
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions{
                AllowAutoRedirect = false,
                BaseAddress = new Uri("https://localhost:5001")
            });
        }
        [Fact]
        public async Task Delete_DeleteUser_WhenCalled_ThanReturnsNoContent(){
            //Arrange Act
            var response = await _client.DeleteAsync("/api/user?id=2");
            //Assert
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }
        [Fact]
        public async Task Delete_DeleteUser_WhenCalledWithInvalidId_ThanReturnsBadRequest(){
            //Arrange Act
            var response = await _client.DeleteAsync("/api/user?id=200000");
            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}