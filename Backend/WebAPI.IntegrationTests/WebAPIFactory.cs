using System;
using System.Linq;
using DAL.Persistance.Context.Seeds;
using DAL.Persistance.Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace WebAPI.IntegrationTests
{
    public class WebAPIFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            ConfigureServices(services));
        }
        public void ConfigureServices(IServiceCollection services)
        {
            var descriptor = services.SingleOrDefault(
                           d => d.ServiceType ==
                               typeof(DbContextOptions<AppDbContext>));

            services.Remove(descriptor);

            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDbForTesting");
            });

            var sp = services.BuildServiceProvider();

            using (var scope = sp.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<AppDbContext>();
                var logger = scopedServices
                    .GetRequiredService<ILogger<WebAPIFactory<TStartup>>>();

                db.Database.EnsureCreated();

                try
                {
                    db.Users.AddRange(new UserSeeds().Users);
                    db.Tasks.AddRange(new TaskSeeds().Tasks);
                    db.Projects.AddRange(new ProjectSeeds().Projects);
                    db.Teams.AddRange(new TeamSeeds().Teams);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "An error occurred seeding the " +
                        "database with test messages. Error: {Message}", ex.Message);
                }
            }
        }
    }
}