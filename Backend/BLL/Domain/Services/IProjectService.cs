using System.Collections.Generic;
using Common.DTO.Project;
using System.Threading.Tasks;
using System.Threading;

namespace BLL.Domain.Services
{
    public interface IProjectService
    {
        Task<ProjectDTO> CreateProject(CreateProjectDTO createProjectDTO);
        Task DeleteProject(int id);
        Task<IEnumerable<ProjectDTO>> GetAllProjects(CancellationToken? token);
        Task<ProjectDTO> GetById(int id);
        Task UpdateProject(ProjectDTO updateDto);
    }

}