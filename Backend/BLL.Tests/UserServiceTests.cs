using System;
using BLL.Services;
using Common.DTO.User;
using Xunit;
using FakeItEasy;
using AutoMapper;
using BLL.MappingProfiles;
using Newtonsoft.Json;
using System.Collections.Generic;
using DAL.Domain.Repositories;
using DAL.Domain.Models;
using System.Threading.Tasks;
using BLL.Tests.Seeds;
using BLL.Tests.FakeRepository;

namespace BLL.Tests
{
    public class UserServiceTests
    {
        private UserService _service;
        private IRepository<UserModel> _userRepo;
        private IRepository<TeamModel> _teamRepo;
        public UserServiceTests()
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UserProfile());
            }).CreateMapper();
            _userRepo = A.Fake<IRepository<UserModel>>();
            _teamRepo = new FakeTeamRepo();
            _service = new UserService(mapper, _userRepo, _teamRepo);
        }
        public static IEnumerable<object[]> ValidUsers()
        {
            // yield return new object[]{ new CreateUserDTO {
            //     FirstName = "John",
            //     LastName = "Dou",
            //     Email = "John.Dou@mail.com",
            //     BirthDay = new DateTime(2003, 12, 30)
            // }};
            yield return new object[]{new CreateUserDTO {
                FirstName = "Jill",
                LastName = "Johnson",
                TeamId = 5,
                Email = "Jill.Johnson@mail.com",
                BirthDay = new DateTime(2003, 12, 30)
            }};
        }

        [Theory]
        [MemberData(nameof(ValidUsers))]
        public async Task CreateUser_WhenModelValid_ThanCreated(CreateUserDTO createUserDTO)
        {
            //Arrange Act
            var userDTO = await _service.CreateUser(createUserDTO);
            //Assert
            Assert.NotNull(userDTO);
        }
        public static IEnumerable<object[]> InvalidUsers()
        {
            yield return new object[]{ new CreateUserDTO{
                FirstName = "John",
                LastName = "Dou",
                Email = "Jon.Dou-mail.com"
            }};
            yield return new object[]{ new CreateUserDTO{
                FirstName = "",
                LastName = "Dou",
                Email = "Dou@mail.com"
            }};
            yield return new object[]{ new CreateUserDTO{
                FirstName = "John",
                LastName = "Dou",
                TeamId = 1010,
                Email = "Dou@mail.com"
            }};
            yield return new object[]{ new CreateUserDTO{
                FirstName = "",
                LastName = "",
                TeamId = 12,
                Email = "🤨"
            }};
        }
        [Theory]
        [MemberData(nameof(InvalidUsers))]
        public async Task CreateUser_WhenModelInvalid_ThanThrowArgumentException(CreateUserDTO createUserDTO)
        {
            //Arrange Act Assert
            await Assert.ThrowsAsync<ArgumentException>(() => _service.CreateUser(createUserDTO));
        }
    }
}