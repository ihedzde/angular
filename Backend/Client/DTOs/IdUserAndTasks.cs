using System.Collections.Generic;
using System.Text;
using Client.Domain.Models;

namespace Client.DTOs
{
    public class IdUserAndTasks
    {
        public int Id { get; set; }
        public UserModel User { get; set; }
        public IList<TaskModel> Tasks { get; set; }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (var task in Tasks)
            {
                builder.AppendLine(task.ToString());
            }

            return $"Id: {Id} User: {User} Members: {builder.ToString()}";
        }
    }
}