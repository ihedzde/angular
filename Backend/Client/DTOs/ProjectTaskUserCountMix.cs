using System.Text;
using Client.Domain.Models;

namespace Client.DTOs
{
    public class ProjectTaskUserCountMix
    {
        public ProjectModel Project { get; set; }
        public TaskModel LongestDescriptionTask { get; set; }
        public TaskModel WithShortestNameTask { get; set; }
        public int? TotalTeamUsersCount { get; set; }
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine($"Project:\n {Project}");
            stringBuilder.AppendLine($"Task with longest Desciption:\n {LongestDescriptionTask?.ToString()}");
            stringBuilder.AppendLine($"Task with shortest name:\n {WithShortestNameTask?.ToString()}");
            if(TotalTeamUsersCount != null)
                stringBuilder.AppendLine($"TotalTeamUsersCount: {TotalTeamUsersCount}");
            return stringBuilder.ToString();
        }
    }
}