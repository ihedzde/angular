using Newtonsoft.Json;
using Client.Domain.Configs;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.Domain.Interfaces;
using Client.Domain.Models;
using System.Linq;
using System.Text;
using System.Net;
using Common.DTO.Project;

namespace Client.Domain.Networking
{
    public class ProjectRepository : IDisposable, IRepository<ProjectModel, CreateProjectDTO>
    {
        private readonly HttpClient _client;
        private readonly string _route = Routes.Project.ToString();

        public ProjectRepository()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Config.FetchUrlBase);
        }

        public void Dispose()
        {
            _client.Dispose();
        }
        public async Task<ProjectModel> Create(CreateProjectDTO data)
        {
            var json = JsonConvert.SerializeObject(data);
            var response = await _client.PostAsync(_route, new StringContent(json, Encoding.UTF8, "application/json"));
            if (response.StatusCode == HttpStatusCode.BadRequest)
                throw new ArgumentException(await response.Content.ReadAsStringAsync());
            if (response.StatusCode == HttpStatusCode.PreconditionFailed)
                throw new InvalidOperationException(await response.Content.ReadAsStringAsync());
            return JsonConvert.DeserializeObject<ProjectModel>(await response.Content.ReadAsStringAsync());
        }
        public async Task<IList<ProjectModel>> Read()
        {
            var json = (await _client.GetAsync(_route)).Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IList<ProjectModel>>(await json);
        }
        public async Task Update(ProjectModel data)
        {
             await _client.PutAsync(_route, new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
        }
        public async Task Delete(int id)
        {
            await _client.DeleteAsync(_route + $"?id={id}");
        }
    }
}
