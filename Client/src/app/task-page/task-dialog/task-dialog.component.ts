import { TaskState } from './../../models/task-enum';
import { CreateTask } from './../../models/create-task';
import { Component, OnInit, Inject, NgModule } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Task } from '../../models/task';
@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.scss']
})
export class TaskDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TaskDialogComponent>, @Inject(MAT_DIALOG_DATA) public task: Task) {
  }
  data: CreateTask | Task = { name: '', description: '', state: TaskState.Backlog, performerId: 0, projectId: 0 }
  ngOnInit(): void {
    if (this.task != null) {
      this.data = this.task;
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
