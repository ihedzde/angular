import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TaskService } from '../services/task-service.service';
import { Task } from './../models/task';
import { TaskDialogComponent } from './task-dialog/task-dialog.component';
@Component({
  selector: 'app-task-page',
  templateUrl: './task-page.component.html',
  styleUrls: ['./task-page.component.scss']
})
export class TaskPageComponent implements OnInit, OnDestroy {
  navigationSubscription: Subscription;
  constructor(private route: ActivatedRoute, private taskService: TaskService) {
    this.route.params.pipe(
      switchMap(params => {
        const reloadToken = params['reloadToken'];
        if (reloadToken !== 'none')
          this.tasks$ = this.taskService.getTasks$();
        return this.tasks$;
      }),
    );
  }
  public tasks$: Observable<Task[]>;


  ngOnInit(): void {
    this.tasks$ = this.taskService.getTasks$();
  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }
}
