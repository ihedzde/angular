import { Component, OnInit, Inject, NgModule, InjectionToken } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { FormComponent } from 'src/app/guard/form.guard';
import { CreateTask } from 'src/app/models/create-task';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task-service.service';

@Component({
  selector: 'app-task-dialog',
  templateUrl: './update-task-dialog.component.html',
  styleUrls: ['./update-task-dialog.component.scss']
})

export class UpdateTaskDialogComponent implements OnInit, FormComponent {

  constructor(public taskService: TaskService, private router: Router,
    private route: ActivatedRoute) {
  }
  submitedOrGood: boolean = false;
  canExit(): boolean | Observable<boolean> | Promise<boolean> {
    if(this.submitedOrGood)
      return true;
    if (confirm("Do you want to leave?")) {
      return true
    } else {
      return false
    }
  }
  task$: Observable<Task>;
  ngOnInit(): void {
   this.task$ = this.route.params.pipe(
      switchMap(params => {
        const id = +params['id'];
        return this.taskService.getById$(id);
      }),
    );
  }
  onSubmit(data:Task) {
    debugger;
    this.submitedOrGood = true;
    this.taskService.updateTask$(data).subscribe();
    this.router.navigate(['../../../updated'], { relativeTo: this.route }).then(()=>window.location.reload());
  }
  onNoClick(updateForm: NgForm): void {
    this.submitedOrGood = (!updateForm.dirty && updateForm.untouched)!;
    this.router.navigate(['../../../none'], { relativeTo: this.route });
  }
}
