import { CreateTeam } from './../../models/create-team';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormComponent } from 'src/app/guard/form.guard';
import { TeamService } from 'src/app/services/team.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-team-dialog',
  templateUrl: './create-team-dialog.component.html',
  styleUrls: ['./create-team-dialog.component.scss']
})
export class CreateTeamDialogComponent implements OnInit, FormComponent {

  constructor(public teamService: TeamService, private router: Router,
    private route: ActivatedRoute) {
  }
  submitedOrGood: boolean = false;
  canExit(): boolean | Observable<boolean> | Promise<boolean> {
    if(this.submitedOrGood)
      return true;
    if (confirm("Do you want to leave?")) {
      return true
    } else {
      return false
    }
  }
  team: CreateTeam = {name:''};
  ngOnInit(): void {
  }
  onSubmit(data:CreateTeam) {
    this.submitedOrGood = true;
    this.teamService.createTeam$(data).subscribe();
    this.router.navigate(['../../created'], { relativeTo: this.route }).then(()=>window.location.reload());
  }
  onNoClick(updateForm: NgForm): void {
    this.submitedOrGood = (!updateForm.dirty && updateForm.untouched)!;
    this.router.navigate(['../../none'], { relativeTo: this.route });
  }
}
