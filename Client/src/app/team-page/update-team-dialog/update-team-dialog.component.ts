import { Component, OnInit, Inject, NgModule, InjectionToken } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { FormComponent } from 'src/app/guard/form.guard';
import { CreateTeam } from 'src/app/models/create-team';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';
@Component({
  selector: 'app-team-dialog',
  templateUrl: './update-team-dialog.component.html',
  styleUrls: ['./update-team-dialog.component.scss']
})

export class UpdateTeamDialogComponent implements OnInit, FormComponent {

  constructor(public teamService: TeamService, private router: Router,
    private route: ActivatedRoute) {
  }
  submitedOrGood: boolean = false;
  canExit(): boolean | Observable<boolean> | Promise<boolean> {
    if(this.submitedOrGood)
      return true;
    if (confirm("Do you want to leave?")) {
      return true
    } else {
      return false
    }
  }
  team$: Observable<Team>;
  ngOnInit(): void {
   this.team$ = this.route.params.pipe(
      switchMap(params => {
        const id = +params['id'];
        return this.teamService.getById$(id);
      }),
    );
  }
  onSubmit(data:Team) {
    this.submitedOrGood = true;
    this.teamService.updateTeam$(data).subscribe();
    this.router.navigate(['../../../updated'], { relativeTo: this.route }).then(()=>window.location.reload());
  }
  onNoClick(updateForm: NgForm): void {
    this.submitedOrGood = (!updateForm.dirty && updateForm.untouched)!;
    this.router.navigate(['../../../none'], { relativeTo: this.route });
  }
}
