import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Team } from '../models/team';
import { TeamService } from '../services/team.service';
@Component({
  selector: 'app-team-page',
  templateUrl: './team-page.component.html',
  styleUrls: ['./team-page.component.scss']
})
export class TeamPageComponent implements OnInit, OnDestroy {
  navigationSubscription: Subscription;
  constructor(private route: ActivatedRoute, private teamService: TeamService) {
    this.route.params.pipe(
      switchMap(params => {
        const reloadToken = params['reloadToken'];
        if (reloadToken !== 'none')
          this.teams$ = this.teamService.getTeams$();
        return this.teams$;
      }),
    );
  }
  public teams$: Observable<Team[]>;


  ngOnInit(): void {
    this.teams$ = this.teamService.getTeams$();
  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }
}
