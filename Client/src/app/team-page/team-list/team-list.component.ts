import { Team } from './../../models/team';
import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss']
})
export class TeamListComponent implements OnInit{

  @Input('teams$')
  public teams$: Observable<Team[]>;
  constructor() {
  }
  ngOnInit(): void {

  }
}
