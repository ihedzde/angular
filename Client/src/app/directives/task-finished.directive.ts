import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appTaskFinished]'
})
export class TaskFinishedDirective {

  constructor(el: ElementRef<HTMLElement>) {
    let child = document.createElement('span');
    child.textContent = "✅";
    el.nativeElement.appendChild(child);
  }

}
