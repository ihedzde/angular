import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appProjectDeadline]'
})
export class ProjectDeadlineDirective {

  constructor(el: ElementRef<HTMLElement>) {
    let child = document.createElement('span');
    child.textContent = "🔥";
    el.nativeElement.style.backgroundColor = 'black';
    el.nativeElement.style.color = 'white';
    el.nativeElement.style.fontWeight = 'bold';
    
    el.nativeElement.appendChild(child);
  }
}
