import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Project } from '../models/project';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { CreateProject } from '../models/create-project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private route = 'https://localhost:5001/api/project';
  constructor(private http: HttpClient) { }

  getProjects$(): Observable<Project[]> {
    return this.http.get<Project[]>(this.route);
  }
  createProject$(project: CreateProject): Observable<Project> {
    const headers = { 'content-type': 'application/json' };
    return this.http.post<Project>(this.route, JSON.stringify(project), { headers: headers });
  }
  getById$(id:number): Observable<Project>{
    return this.http.get<Project>(this.route+`/${id}`);
  }
  updateProject$(project: Project): Observable<any> {
    const headers = { 'content-type': 'application/json' };
    return this.http.put(this.route, JSON.stringify(project), { headers: headers });
  }
}
