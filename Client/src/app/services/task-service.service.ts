import { CreateTask } from './../models/create-task';
import { Task } from './../models/task';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private route = 'https://localhost:5001/api/task';
  constructor(private http: HttpClient) { }

  getTasks$(): Observable<Task[]> {
    return this.http.get<Task[]>(this.route);
  }
  createTask$(task: CreateTask): Observable<Task> {
    const headers = { 'content-type': 'application/json' };
    task.state = parseInt(task.state.toString());
    return this.http.post<Task>(this.route, JSON.stringify(task), { headers: headers });
  }
  getById$(id:number): Observable<Task>{
    return this.http.get<Task>(this.route+`/${id}`);
  }
  updateTask$(task: Task): Observable<any> {
    const headers = { 'content-type': 'application/json' };
    task.state = parseInt(task.state.toString());
    if (task.state == 2) {
      task.finishedAt = new Date();
    } else {
      task.finishedAt = null;
    }
    return this.http.put(this.route, JSON.stringify(task), { headers: headers });
  }
}
