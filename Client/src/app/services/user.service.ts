import { CreateUser } from './../models/create-user';
import { User } from './../models/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private route = 'https://localhost:5001/api/user';
  constructor(private http: HttpClient) { }

  getUsers$(): Observable<User[]> {
    return this.http.get<User[]>(this.route);
  }
  createUser$(user: CreateUser): Observable<User> {
    const headers = { 'content-type': 'application/json' };
    return this.http.post<User>(this.route, JSON.stringify(user), { headers: headers });
  }
  getById$(id:number): Observable<User>{
    return this.http.get<User>(this.route+`/${id}`);
  }
  updateUser$(user: User): Observable<any> {
    const headers = { 'content-type': 'application/json' };
    return this.http.put(this.route, JSON.stringify(user), { headers: headers });
  }
}
