import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Project } from '../models/project';
import { ProjectService } from '../services/project.service';
@Component({
  selector: 'app-project-page',
  templateUrl: './projectpage.component.html',
  styleUrls: ['./projectpage.component.scss']
})
export class ProjectPageComponent implements OnInit, OnDestroy {
  navigationSubscription: Subscription;
  constructor(private route: ActivatedRoute, private projectService: ProjectService) {
    this.route.params.pipe(
      switchMap(params => {
        const reloadToken = params['reloadToken'];
        if (reloadToken !== 'none')
          this.projects$ = this.projectService.getProjects$();
        return this.projects$;
      }),
    );
  }
  public projects$: Observable<Project[]>;


  ngOnInit(): void {
    this.projects$ = this.projectService.getProjects$();
  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }
}
