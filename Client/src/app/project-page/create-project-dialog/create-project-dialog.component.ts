import { CreateProject } from './../../models/create-project';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormComponent } from 'src/app/guard/form.guard';
import { ProjectService } from 'src/app/services/project.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-project-dialog',
  templateUrl: './create-project-dialog.component.html',
  styleUrls: ['./create-project-dialog.component.scss']
})
export class CreateProjectDialogComponent implements OnInit, FormComponent {

  constructor(public projectService: ProjectService, private router: Router,
    private route: ActivatedRoute) {
  }
  submitedOrGood: boolean = false;
  canExit(): boolean | Observable<boolean> | Promise<boolean> {
    if (this.submitedOrGood)
      return true;
    if (confirm("Do you want to leave?")) {
      return true
    } else {
      return false
    }
  }
  project: CreateProject = { name: '', authorId: 0, teamId: 0, tasks: [], description: '' };
  ngOnInit(): void {
  }
  onSubmit(data: CreateProject) {
    this.submitedOrGood = true;
    this.projectService.createProject$(data).subscribe();
    this.router.navigate(['../../created'], { relativeTo: this.route }).then(() => window.location.reload());
  }
  onNoClick(updateForm: NgForm): void {
    this.submitedOrGood = (!updateForm.dirty && updateForm.untouched)!;
    this.router.navigate(['../../none'], { relativeTo: this.route });
  }
}
