import { Component, OnInit, Inject, NgModule, InjectionToken } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { FormComponent } from 'src/app/guard/form.guard';
import { CreateProject } from 'src/app/models/create-project';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-dialog',
  templateUrl: './update-project-dialog.component.html',
  styleUrls: ['./update-project-dialog.component.scss']
})

export class UpdateProjectDialogComponent implements OnInit, FormComponent {

  constructor(public projectService: ProjectService, private router: Router,
    private route: ActivatedRoute) {
  }
  submitedOrGood: boolean = false;
  canExit(): boolean | Observable<boolean> | Promise<boolean> {
    if(this.submitedOrGood)
      return true;
    if (confirm("Do you want to leave?")) {
      return true
    } else {
      return false
    }
  }
  project$: Observable<Project>;
  ngOnInit(): void {
   this.project$ = this.route.params.pipe(
      switchMap(params => {
        const id = +params['id'];
        return this.projectService.getById$(id);
      }),
    );
  }
  onSubmit(data:Project) {
    debugger;
    this.submitedOrGood = true;
    this.projectService.updateProject$(data).subscribe();
    this.router.navigate(['../../../updated'], { relativeTo: this.route }).then(()=>window.location.reload());
  }
  onNoClick(updateForm: NgForm): void {
    this.submitedOrGood = (!updateForm.dirty && updateForm.untouched)!;
    this.router.navigate(['../../../none'], { relativeTo: this.route });
  }
}
