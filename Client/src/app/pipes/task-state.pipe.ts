import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'taskState'
})
export class TaskStatePipe implements PipeTransform {

  transform(value: number): string | number {
    switch (value) {
      case 0: return " Development";
      case 1: return "Testing";
      case 2: return "Finished";
      case 3: return "Backlog";
      default: return value;
    }
  }

}
