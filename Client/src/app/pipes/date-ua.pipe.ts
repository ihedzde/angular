import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
@Pipe({
  name: 'dateUA'
})
export class DateUAPipe implements PipeTransform {

  constructor(public datepipe: DatePipe) { }
  transform(value: Date | undefined): string | undefined {
    if (value == null)
      return 'No data 🤷🏻‍♂️.';
    return this.datepipe.transform(value, 'dd LLLL yyyy', undefined, "ua")?.replace('ад', 'ада')
    .replace('ень', 'ня').replace('ий', 'ого');
  }

}
