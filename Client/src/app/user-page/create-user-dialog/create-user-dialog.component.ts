import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormComponent } from 'src/app/guard/form.guard';
import { CreateUser } from 'src/app/models/create-user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-create-user-dialog',
  templateUrl: './create-user-dialog.component.html',
  styleUrls: ['./create-user-dialog.component.scss']
})
export class CreateUserDialogComponent implements OnInit, FormComponent {

  constructor(public userService: UserService, private router: Router,
    private route: ActivatedRoute) {
  }
  submitedOrGood: boolean = false;
  canExit(): boolean | Observable<boolean> | Promise<boolean> {
    if(this.submitedOrGood)
      return true;
    if (confirm("Do you want to leave?")) {
      return true
    } else {
      return false
    }
  }
  user: CreateUser = {firstName:'', lastName:'', birthDay: new Date(), email:''};
  ngOnInit(): void {
  }
  onSubmit(data:CreateUser) {
    this.submitedOrGood = true;
    this.userService.createUser$(data).subscribe();
    this.router.navigate(['../../created'], { relativeTo: this.route }).then(()=>window.location.reload());
  }
  onNoClick(updateForm: NgForm): void {
    this.submitedOrGood = (!updateForm.dirty && updateForm.untouched)!;
    this.router.navigate(['../../none'], { relativeTo: this.route });
  }

}
