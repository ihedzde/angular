import { CreateTaskDialogComponent } from './task-page/create-task-dialog/create-task-dialog.component';
import { UpdateTaskDialogComponent } from './task-page/update-task-dialog/update-task-dialog.component';
import { CreateUserDialogComponent } from './user-page/create-user-dialog/create-user-dialog.component';
import { UpdateUserDialogComponent } from './user-page/update-user-dialog/update-user-dialog.component';
import { FormGuard } from './guard/form.guard';
import { UserPageComponent } from './user-page/user-page.component';
import { TeamPageComponent } from './team-page/team-page.component';
import { TaskPageComponent } from './task-page/task-page.component';
import { ProjectPageComponent } from './project-page/projectpage.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateTeamDialogComponent } from './team-page/create-team-dialog/create-team-dialog.component';
import { UpdateTeamDialogComponent } from './team-page/update-team-dialog/update-team-dialog.component';
import { UpdateProjectDialogComponent } from './project-page/update-project-dialog/update-project-dialog.component';
import { CreateProjectDialogComponent } from './project-page/create-project-dialog/create-project-dialog.component';

const routes: Routes = [
  {
    path: 'projects/:reloadToken', component: ProjectPageComponent,
    children: [
      {
        path: 'update/:id', component: UpdateProjectDialogComponent,
        canDeactivate: [FormGuard],
      },
      {
        path: 'create', component: CreateProjectDialogComponent, canDeactivate: [FormGuard],
      }
    ],
  },
  {
    path: 'tasks/:reloadToken', component: TaskPageComponent,
    children: [
      {
        path: 'update/:id', component: UpdateTaskDialogComponent,
        canDeactivate: [FormGuard],
      },
      {
        path: 'create', component: CreateTaskDialogComponent, canDeactivate: [FormGuard],
      }
    ],
  },
  {
    path: 'teams/:reloadToken', component: TeamPageComponent,
    children: [
      {
        path: 'update/:id', component: UpdateTeamDialogComponent,
        canDeactivate: [FormGuard],
      },
      {
        path: 'create', component: CreateTeamDialogComponent, canDeactivate: [FormGuard],
      }
    ],
  },
  {
    path: 'users/:reloadToken', component: UserPageComponent,
    children: [
      {
        path: 'update/:id', component: UpdateUserDialogComponent,
        canDeactivate: [FormGuard],
      },
      {
        path: 'create', component: CreateUserDialogComponent, canDeactivate: [FormGuard],
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
