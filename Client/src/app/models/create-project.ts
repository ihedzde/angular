import { Task } from './task';
import { Team } from './team';
import { User } from './user';
export interface CreateProject {
    authorId: number,
    teamId: number,
    name: string,
    tasks: Task[],
    description: string,
    deadline?: Date,
}